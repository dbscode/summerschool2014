#!/bin/sh

cd "`dirname \"$0\"`"
SOURCE_ROOT_PATH=$PWD

#############
# C++ Tests #
#############

cd $SOURCE_ROOT_PATH/wristband/demo_tests
make
if [ $? != 0 ]; then
	echo "Failed to build demo_tests"
	exit 1
fi

./unittests
if [ $? != 0 ]; then
	echo "Failing test in demo_tests project"
	exit 1
fi

#############
# Web tests #
#############

cd $SOURCE_ROOT_PATH/web

phpunit --verbose .
if [ $? != 0 ]; then
	echo "Failing test in web project"
	exit 1
fi

exit 0
