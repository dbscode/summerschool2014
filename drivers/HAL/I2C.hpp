#pragma once

#include <exception>

class I2C
{
public:
	virtual ~I2C() { }
	virtual bool SetAddress(unsigned char address) = 0;
	virtual bool Read(void * buffer, int length) = 0;
	virtual bool Write(const void * buffer, int length) = 0;
};

class I2CUnableToOpenException : public std::exception
{
};
