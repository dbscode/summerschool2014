#pragma once

#include "Gyroscope.hpp"

class I2C;

class ITG3200Gyroscope : public Gyroscope
{
public:
	explicit ITG3200Gyroscope(I2C & i2c);
	virtual Raw3DSensorData ReadGyroscope() const;

private:
	I2C & i2c;
};
