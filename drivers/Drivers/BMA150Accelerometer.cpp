#include "BMA150Accelerometer.hpp"
#include "Raw3DSensorData.hpp"
#include "HAL/I2C.hpp"

BMA150Accelerometer::BMA150Accelerometer(I2C & i2c)
	: i2c(i2c)
{
}

Raw3DSensorData BMA150Accelerometer::ReadAcceleration() const
{
	const unsigned char BMA150Address = 0x38;
	i2c.SetAddress(BMA150Address);

	unsigned char registerAddress[] = { 0x02 };
	i2c.Write(registerAddress, sizeof(registerAddress));

	Raw3DSensorData result;
	i2c.Read(&result, sizeof(result));
	result.x >>= 6;
	result.y >>= 6;
	result.z >>= 6;

	return result;
}
