#include "AK8975Compass.hpp"
#include "Raw3DSensorData.hpp"
#include "HAL/I2C.hpp"

const unsigned char AK8975Address = 0x0C;

AK8975Compass::AK8975Compass(I2C & i2c)
	: i2c(i2c)
{
}

void AK8975Compass::ReadRegisters(
	unsigned char startRegister,
	void * buffer,
	unsigned char numberOfRegistersToRead) const
{
	i2c.Write(&startRegister, sizeof(startRegister));
	i2c.Read(buffer, numberOfRegistersToRead);
}

void AK8975Compass::StartDirectionRead() const
{
	i2c.SetAddress(AK8975Address);

	const unsigned char startCommand[] = { 0x0A, 0x01 };
	i2c.Write(startCommand, sizeof(startCommand));
}

bool AK8975Compass::IsReadingComplete() const
{
	i2c.SetAddress(AK8975Address);

	unsigned char status1;
	ReadRegisters(0x02, &status1, sizeof(status1));

	return status1;
}

Raw3DSensorData AK8975Compass::ReadDirection() const
{
	i2c.SetAddress(AK8975Address);

	Raw3DSensorData result;
	ReadRegisters(0x03, &result, sizeof(result));

	return result;
}

unsigned char AK8975Compass::ReadDeviceID() const
{
	i2c.SetAddress(AK8975Address);

	unsigned char compassID;
	ReadRegisters(0x00, &compassID, sizeof(compassID));

	return compassID;
}
