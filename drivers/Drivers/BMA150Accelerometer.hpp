#pragma once

#include "Accelerometer.hpp"

class I2C;

class BMA150Accelerometer : public Accelerometer
{
public:
	explicit BMA150Accelerometer(I2C & i2c);
	virtual Raw3DSensorData ReadAcceleration() const;

private:
	I2C & i2c;
};

class BMA150AccelerometerCommunicationsError : public AccelerometerException
{
};
