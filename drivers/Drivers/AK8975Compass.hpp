#pragma once

#include "Compass.hpp"

class I2C;

class AK8975Compass : public Compass
{
public:
	explicit AK8975Compass(I2C & i2c);

	virtual void StartDirectionRead() const;
	virtual bool IsReadingComplete() const;
	virtual Raw3DSensorData ReadDirection() const;

	unsigned char ReadDeviceID() const;

private:
	I2C & i2c;

	void ReadRegisters(unsigned char startRegister, void* buffer, unsigned char numberOfRegistersToRead) const;
};
