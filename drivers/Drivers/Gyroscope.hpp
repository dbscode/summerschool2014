#pragma once

struct Raw3DSensorData;

class Gyroscope
{
public:
	virtual ~Gyroscope() { }
	virtual Raw3DSensorData ReadGyroscope() const = 0;
};
