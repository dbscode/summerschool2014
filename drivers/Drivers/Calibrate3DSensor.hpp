#pragma once

#include "Calibrated3DSensorData.hpp"

struct Raw3DSensorData;

struct AxisCalibration
{
	short bias;
	float scale;
};

struct Calibration
{
	AxisCalibration x;
	AxisCalibration y;
	AxisCalibration z;
};

class Calibrate3DSensor
{
public:
	Calibrate3DSensor(const Calibration & calibration);
	Calibrated3DSensorData Calibrate(const Raw3DSensorData & raw) const;

private:
	Calibration _calibration;
};
